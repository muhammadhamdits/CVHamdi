<!DOCTYPE html>
<html>
<head>
	<title>CV Muhammad Hamdi</title>
	<link rel="shortcut icon" href="icon.png">
	<style type="text/css">
		#isi {
			background-image: url('foto2.jpg');
		}
	</style>
</head>
<body>
	<div id="header" style="height: 40px; background-color: #6495ED	; width: 100%" >
	</div>

	

	<div id="isi">
		<br><br><br>
		<div id="left" style="height: 1510px;background-color: #F0FFF0 ;width: 31%;margin-left: 100px">
			<div id="hl1" style="height: 35px;background-color: #6495ED; width: 100%">
				<div style="height: 4px;background-color: #6495ED;width: 100%"></div>
				<b style="font-size: 24px;color: white; margin-left: 75px;">MUHAMMAD HAMDI</b>
			</div>
			<img src="foto1.jpg" alt="Muhammad Hamdi" height="300" width="300" style="margin-left: 50px;margin-top: 50px; border-radius: 50%">
			<br><br><br><br>

			<div id="hl2" style="height: 35px;background-color: #6495ED;width: 100%">
				<div style="height: 3px;background-color: #6495ED; width: 100%"></div>
				<img src="icon1.ico" height="20" width="20" style="margin-left: 10px;">
				<b style="font-size: 24px;color: white;margin-left: 5px">Data Pribadi</b>
			</div><br>
			<p style="margin-left: 15px; margin-top: 15px;font-size: 18px">
				Tempat/tanggal lahir &nbsp: Padang/03 Februari 2000<br><br>
				Jenis kelamin&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: Laki-laki<br><br>
				Agama&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: Islam<br><br>
				Warga Negara&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: Indonesia<br><br>
				Status&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: Single<br><br>
				Alamat&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: Jl. Piai Tanah Sirah nan xx<br>
				&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspNo. 46, Lubuk begalung,<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspPadang.<br><br>
			</p><br>

			<div id="hl3" style="height: 35px; background-color: #6495ED;width: 100%">
				<div style="height: 3px;background-color: #6495ED;width: 100%"></div>
				<img src="icon2.ico" height="17" width="17" style="margin-left: 10px;margin-top: 5px">
				<b style="font-size: 24px;color: white;margin-left: 5px">Kontak</b>
			</div><br>
			<p style="margin-left: 15px;margin-top: 15px;font-size: 18px">
				Telepon : +62823-8542-4220<br><br>
				E-mail &nbsp&nbsp: muhammadhamdi702@gmail.com<br><br>
				Sosmed&nbsp: muhammadhamdits<br><br>
				Blog&nbsp&nbsp&nbsp&nbsp&nbsp : <a href="http://mhamdits.wordpress.com">mhamdits.wordpress.com</a> <br>
			</p><br>

			<div id="hl4" style="height: 35px;background-color: #6495ED;width: 100%">
				<div style="height: 3px;background-color: #6495ED;width: 100%"></div>
				<img src="icon3.png" height="17" width="17" style="margin-left: 10px;margin-top: 5px">
				<b style="font-size: 24px;color: white;margin-left: 5px">Kemampuan</b>
			</div><br>
			<p style="margin-left: 15px;margin-top: 15px;font-size: 18px">
				* Sistem Operasi<br>
				Microsoft Windows XP/7/8/8.1/10, Ubuntu.<br><br>
				* Bahasa Pemrograman<br>
				C++.<br><br>
				* Database.<br>
				MySQL.<br><br>
				* Software<br>
				Microsoft Office, Adobe Photoshop, Corel Draw, dll.<br><br>
				* Hardware<br>
				Merakit dan instalasi PC.<br>
			</p>
		</div>

		<div style="margin-left: 700px; margin-top: -1500px;">
			<b style="font-size: 35px">CURRICULLUM VITAE</b>		
		</div>

		<br><br>

		<div id="right" style="height: 1410px; background-color: #F0FFF0; width: 51%; margin-left: 560px;margin-top: 10px">			
			<div id="hr1" style="height: 35px;background-color: #6495ED; width: 100%">
				<div style="height: 3px;background-color: #6495ED;width: 100%"></div>
				<img src="icon4.png" height="20" width="20" style="margin-left: 10px;margin-top: 5px">
				<b style="font-size: 24px;color: white; margin-left: 5px;">Pengalaman Organisasi</b>
			</div><br>
			<p style="margin-left: 15px;margin-top: 15px;font-size: 18px">
				- Peserta OR LEA &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2018<br><br>
				- Peserta OR Pantasio &nbsp&nbsp&nbsp&nbsp2018<br>
			</p><br>
			<div id="hr2" style="height: 35px;background-color: #6495ED;width: 100%">
				<div style="height: 3px;background-color: #6495ED;width: 100%"></div>
				<img src="icon5.svg" height="20" width="20" style="margin-left: 10px;margin-top: 5px">
				<b style="font-size: 24px;color: white;margin-left: 5px">Pendidikan</b>
			</div><br>
			<p style="margin-left: 15px;margin-top: 15px;font-size: 18px">
				<b style="font-size: 30px;margin-left: 275px">Formal</b>
				<table width="650" style="margin-right: : 20px;font-size: 20px" cellspacing="30">
					<th style="font-size: 25px">Institusi</th>
					<th style="font-size: 25px">Tahun</th>
					<tr>
						<td><b>1.&nbsp&nbsp</b> TK Presiden Padang</td>
						<td style="margin-left: 50px">2004-2005</td>
					</tr>
					<tr>
						<td><b>2.&nbsp&nbsp</b> SD Negeri 34 Tanah Sirah Padang</td>
						<td style="margin-left: 50px">2005-2011</td>
					</tr>
					<tr>
						<td><b>3.&nbsp&nbsp</b> MTsN Model Padang</td>
						<td style="margin-left: 50px">2011-2014</td>
					</tr>
					<tr>
						<td><b>4.&nbsp&nbsp</b> MAN 2 Padang</td>
						<td style="margin-left: 50px">2014-2017</td>
					</tr>
					<tr>
						<td><b>5.&nbsp&nbsp</b> Universitas Andalas, Padang. Fakultas Teknologi Informasi, Jurusan Sistem Informasi</td>
						<td style="margin-left: 50px">2017-Sekarang</td>
					</tr>
				</table><br><br>
				<b style="font-size: 30px;margin-left: 250px;">Non-Formal</b>
				<p style="font-size: 25px;text-align: center">Tidak Ada</p>
			</p><br>

			<div id="hr3" style="height: 35px;background-color: #6495ED;width: 100%">
				<div style="height: 3px;background-color: #6495ED;width: 100%"></div>
				<img src="icon6.png" height="20" width="20" style="margin-left: 10px;margin-top: 5px">
				<b style="font-size: 24px;color: white;margin-left: 5px">Prestasi</b>
			</div><br>
			<p style="margin-left: 15px;margin-top: 15px;font-size: 18px">
				<table width="650" style="margin-right: : 20px;font-size: 20px" cellspacing="30">
					<th style="font-size: 25px">Kegiatan</th>
					<th style="font-size: 25px">Tingkat</th>
					<th style="font-size: 25px">Regional</th>
					<th style="font-size: 25px">Tahun</th>
					<tr>
						<td><b>1.&nbsp&nbsp</b>Juara I Kompetisi Sains Madrasah (KSM) Bidang Matematika</td>
						<td>MA</td>
						<td>Padang</td>
						<td>2016</td>
					</tr>
					<tr>
						<td><b>2.&nbsp&nbsp</b>Juara II Kompetisi Sains Madrasah (KSM) Bidang Matematika</td>
						<td>MA</td>
						<td>Sumatera Barat</td>
						<td>2016</td>
					</tr>
				</table>
			</p>
		</div>

	<br><br><br>
	</div>


	<div id="footer" style="height: 40px;background-color: #6495ED; width: 100%">
	</div>
</body>
</html>